#!/bin/bash

fizzbuzz() {
	for((i=1;i<=100;i++)){
		((i%3))||printf 'fizz'
		((i%5))||printf 'buzz'
		((i%3))&&((i%5))&&printf $i
		echo
	}
}

fizzbuzz
